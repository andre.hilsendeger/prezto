## -*- mode: Sh; tab-width: 2; indent-tabs-mode:nil; -*-            ##
######################################################################
## Authors:                                                         ##
##  - Sorin Ionescu <sorin.ionescu@gmail.com>                       ##
##  - Andre Hilsendeger < Andre.Hilsendeger@gmail.com >             ##
## Last-Updated: 2015-08-26                                         ##
##   By: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >          ##
##                                                                  ##
## Filename: zshenv                                                 ##
## Description:                                                     ##
## Defines environment variables.                                   ##
######################################################################
. ~/.env_shared

# Browser
######################################################################
if [[ "$OSTYPE" == darwin* && -z "$BROWSER" ]]; then
  export BROWSER='open'
fi

## Language
######################################################################
[[ -z "$LANG" ]] && export LANG='en_US.UTF-8'

## Paths
######################################################################
typeset -gU cdpath fpath mailpath path

# Set the the list of directories that cd searches.
# cdpath=(
#   $cdpath
# )

# Set the list of directories that Zsh searches for programs.
path=(
    ~/bin/**/
    /opt/*/{bin,sbin}(N)
    /usr/local/{bin,sbin}
    $path
)

## Less
######################################################################
# Set the Less input preprocessor.
if (( $+commands[lesspipe.sh] )); then
  export LESSOPEN='| /usr/bin/env lesspipe.sh %s 2>&-'
fi

# Ensure that a non-login, non-interactive shell has a defined environment.
if [[ "$SHLVL" -eq 1 && ! -o LOGIN && -s "${ZDOTDIR:-$HOME}/.zprofile" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprofile"

fi
